/* 
 * created by senllang 2024/12/4 
 * mail : study@senllang.onaliyun.com 
 *
 * Copyright (c) 2023-2024 senllang

 * This is licensed under Mulan PSL v2.
 * You can use this software according to the terms and conditions of the Mulan PSL v2.
 * You may obtain a copy of Mulan PSL v2 at:
 * http://license.coscl.org.cn/MulanPSL2
 * THIS SOFTWARE IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
 * EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
 * MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
 * See the Mulan PSL v2 for more details.
 * 
 */
#define _GNU_SOURCE
#include <err.h>
#include <errno.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>

static void
display_pthread_attr(pthread_attr_t *attr)
{
    int s;
    size_t stacksize;
    size_t guardsize;
    int policy;
    struct sched_param schedparam;
    int detachstate;
    int inheritsched;

    s = pthread_attr_getstacksize(attr, &stacksize);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Stack size:          %zd\n", stacksize);

    s = pthread_attr_getguardsize(attr, &guardsize);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Guard size:          %zd\n", guardsize);

    s = pthread_attr_getschedpolicy(attr, &policy);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Scheduling policy:   %s\n",
           (policy == SCHED_FIFO) ? "SCHED_FIFO" : (policy == SCHED_RR)  ? "SCHED_RR"
                                               : (policy == SCHED_OTHER) ? "SCHED_OTHER"
                                                                         : "[unknown]");

    s = pthread_attr_getschedparam(attr, &schedparam);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Scheduling priority: %d\n", schedparam.sched_priority);

    s = pthread_attr_getdetachstate(attr, &detachstate);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Detach state:        %s\n",
           (detachstate == PTHREAD_CREATE_DETACHED) ? "DETACHED" : (detachstate == PTHREAD_CREATE_JOINABLE) ? "JOINABLE"
                                                                                                            : "???");

    s = pthread_attr_getinheritsched(attr, &inheritsched);
    if (s != 0)
        exit(EXIT_FAILURE);
    printf("Inherit scheduler:   %s\n",
           (inheritsched == PTHREAD_INHERIT_SCHED) ? "INHERIT" : (inheritsched == PTHREAD_EXPLICIT_SCHED) ? "EXPLICIT"
                                                                                                          : "???");
}

int main(void)
{
    int s;
    pthread_attr_t attr;

    s = pthread_getattr_default_np(&attr);
    if (s != 0)
        exit(EXIT_FAILURE);

    display_pthread_attr(&attr);

    exit(EXIT_SUCCESS);
}