/*
 *	create by senllang 2023/5/3
 *  mail : study@senllang.onaliyun.com
 *	
 */

#ifndef HAT_SH_MEM_H_H
#define HAT_SH_MEM_H_H
#include <sys/types.h>

/* 共享内存的信息结构体 */
typedef struct shareMemInfo
{
	char *pShMemStartAddr;
	int ShMemId;
	int ShMemSize;
	key_t ShMemKey;
}stShMem;

/*
 * 创建指定size大小的共享内存
 *
 * create by senllang 2023/5/2
 *
 * pShmem：传出共享内存的地址
 * size  : 指定共享内存的大小
 */
int CreateShMem(char **pShmem, int size);

/*
 * 解除共享内存的映射，并销毁
 *
 * create by senllang 2023/5/2
 *
 * IsDestroy：是否需要销毁，!=0 时，解除并销毁；=0,只解除映射
 */
int DestroyShMem(int IsDestroy);

#endif
