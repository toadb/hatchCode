/*
 *	create by senllang 2023/5/3
 *  mail : study@senllang.onaliyun.com
 *	
 */

#ifndef HAT_IPC_MUTEX_H_H
#define HAT_IPC_MUTEX_H_H

/*
 * 创建共享互斥量
 *
 * create by senllang 2023/5/2
 *
 * pMemAddr, 共享内存地址
 * Num， 互斥量数量，地址为连续存放
 */
int InitMutex(char *pMemAddr, int Num);

/*
 * 共享互斥量加锁
 *
 * create by senllang 2023/5/2
 *
 * pMemAddr, 共享内存地址
 */
void MutexLock(char *pMemAddr);

/*
 * 解锁共享互斥量
 *
 * create by senllang 2023/5/2
 *
 * pMemAddr, 共享内存地址
 */
void MutexUnlock(char *pMemAddr);

/*
 * 销毁共享互斥量
 *
 * create by senllang 2023/5/2
 *
 * pMemAddr, 共享内存地址
 * Num， 互斥量数量，地址为连续存放
 */
void DestroyMutex(char *pMemAddr, int Num);


/*
 * 获取互斥量内存大小
 *
 * create by senllang 2023/5/2
 *
 * Num， 互斥量数量，地址为连续存放
 */
int GetMutexSize(int Num);


#endif
