/*
 *	create by senllang 2023/5/20
 *  mail : study@senllang.onaliyun.com
 *	
 */

#ifndef HAT_IPC_RWLOCK_H_H
#define HAT_IPC_RWLOCK_H_H

/*
 * 创建读写锁
 *
 * create by senllang 2023/5/20
 *
 * pMemAddr, 共享内存地址
 * Num， 读写锁数量，地址为连续存放
 */
int Initrwlock(char *pMemAddr, int Num);

/*
 * 读写锁加读锁
 *
 * create by senllang 2023/5/20
 *
 * pMemAddr, 共享内存地址
 */
void Rdlock(char *pMemAddr);

/*
 * 读写锁加写锁
 *
 * create by senllang 2023/5/20
 *
 * pMemAddr, 共享内存地址
 */
void Wrlock(char *pMemAddr);

/*
 * 解锁读写锁
 *
 * create by senllang 2023/5/20
 *
 * pMemAddr, 共享内存地址
 */
void RwlockUnlock(char *pMemAddr);


/*
 * 销毁读写锁
 *
 * create by senllang 2023/5/20
 *
 * pMemAddr, 共享内存地址
 * Num， 读写锁数量，地址为连续存放
 */
void DestroyRwlock(char *pMemAddr, int Num);

/*
 * 获取读写锁内存大小
 *
 * create by senllang 2023/5/20
 *
 * Num， 读写锁数量，地址为连续存放
 */
int GetRwlockSize(int Num);

#endif
