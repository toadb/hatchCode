/*
 *	create by senllang 2023/5/3
 *  mail : study@senllang.onaliyun.com
 *	
 */

#ifndef HAT_PROCESS_H_H
#define HAT_PROCESS_H_H

/* 进程类型 */
extern int processType;
/*
 * 进程任务处理主函数，此函数不会返回调用处
 *
 * create by senllang 2023/5/2
 *
 */
int ProcessMain(int argc, char *argv[]);

/*
 * 创建多个子进程
 *
 * 返回值为成功创建子进程的个数
 *
 * create by senllang 2023/5/2
 *
 * Num, 需要创建子进程的个数
 * argc,argv, 传给子进程任务主函数的参数
 *
 */
int CreateSubProcess(int Num, int argc, char *argv[]);

/*
 * 等待子进程结束，或者处理僵尸进程
 *
 * create by senllang 2023/5/2
 *
 */
void WaitSubProcessComplete(void);

#endif
