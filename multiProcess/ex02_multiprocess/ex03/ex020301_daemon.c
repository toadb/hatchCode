/*
 * ex020301_daemon.c
 */
#include <stdio.h>
#include <stdlib.h>

#include <sys/types.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>

void daemon_fork();

int main(int argc ,char *argv[])
{
	daemon_fork();

	sleep(10);

	return 0;
}

void daemon_fork()
{
	int pid = -1;
	pid = fork();
	if(pid < 0)
	{
		printf("fork error[%s]\n",strerror(errno));
		exit(-1);
	}
	else if(pid > 0)
	{
		// parent exit.
		exit(0);
	}
	else 
	{
		// child daemon
		return;
	}
}
