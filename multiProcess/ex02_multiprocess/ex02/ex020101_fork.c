/*
 * ex020101_fork.c
 */
#include <stdio.h>

#include <sys/types.h>
#include <unistd.h>

#include <errno.h>
#include <string.h>

int main(int argc ,char *argv[])
{
	int pid = -1;

	pid = fork();
	if(pid == 0)
	{
		// in child 
		printf("here in child ,my pid is %d\n", getpid());
		sleep(10);
	}
	else if(pid > 0)
	{
		// in parent
		printf("here in parent, my pid %d, child pid is %d\n", getpid(), pid);
		sleep(10);

	}
	else
	{
		// error
		printf("fork error[%s]\n",strerror(errno));
	}

	return 0;
}
